import { sleep} from 'k6';
import { SharedArray } from 'k6/data';
import { BaseRest, BaseCheks, ENDPOINTS, testConfig } from '../../support/base/baseTest.js';

export const options = testConfig.options.smokeTest;

const base_uri = testConfig.environment.hml.url;
const baseRest = new BaseRest(base_uri);
const baseChecks = new BaseCheks();

const data = new SharedArray('Movies', function () {
  const jsonData = JSON.parse(open('../data/dynamic/movies.json'));
  return jsonData.movies;
});

export function setup() {
  let responseData = [];
  data.forEach(movie => {
    const res = baseRest.put(ENDPOINTS.MOVIES_ENDPOINT + `${id}`, movie);
    baseChecks.checkStatusCode(res, 200);
    responseData.push(res.json());
  });
  return { responseData };
}

export default () => {
  let movieIndex = __ITER % data.length;
  let movie = data[movieIndex];
  const urlRes = baseRest.put(ENDPOINTS.MOVIES_ENDPOINT + `${id}`, movie);
  baseChecks.checkStatusCode(urlRes, 200);
  sleep(1);
};

export function teardown(responseData) {
  const ids = responseData.responseData.map(item => item.id);
  ids.forEach(id => {
    const res = baseRest.del(ENDPOINTS.MOVIES_ENDPOINT + `${id}`, movie);
    baseChecks.checkStatusCode(res, 400);
  });
}