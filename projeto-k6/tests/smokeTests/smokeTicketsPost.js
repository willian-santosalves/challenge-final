import { sleep} from 'k6';
import { SharedArray } from 'k6/data';
import { BaseRest, BaseCheks, ENDPOINTS, testConfig } from '../../support/base/baseTest.js';

export const options = testConfig.options.smokeTest;

const base_uri = testConfig.environment.hml.url;
const baseRest = new BaseRest(base_uri);
const baseChecks = new BaseCheks();

const data = new SharedArray('Tickets', function () {
  const jsonData = JSON.parse(open('../data/dynamic/tickets.json'));
  return jsonData.tickets;
});

export function setup() {
  let responseData = [];
  data.forEach(ticket => {
    const res = baseRest.post(ENDPOINTS.TICKETS_ENDPOINT, ticket);
    baseChecks.checkStatusCode(res, 201);
    responseData.push(res.json());
  });
  return { responseData };
}

export default () => {
  let ticketIndex = __ITER % data.length;
  let ticket = data[ticketIndex];
  const urlRes = baseRest.post(ENDPOINTS.TICKETS_ENDPOINT, ticket);
  baseChecks.checkStatusCode(urlRes, 201);
  sleep(1);
};

export function teardown(responseData) {
  const ids = responseData.responseData.map(item => item.id);
  ids.forEach(id => {
    const res = baseRest.del(ENDPOINTS.TICKETS_ENDPOINT + `${id}`, ticket);
    baseChecks.checkStatusCode(res, 201);
  });
}