import { sleep} from 'k6';
import { SharedArray } from 'k6/data';
import { BaseRest, BaseCheks, ENDPOINTS, testConfig } from '../support/base/baseTest.js';

export const options = testConfig.options.smokeThresholds;

const base_uri = testConfig.environment.hml.url;
const baseRest = new BaseRest(base_uri);
const baseChecks = new BaseCheks();

const data = new SharedArray('Movies', function () {
  const jsonData = JSON.parse(open('../data/static/movie.json'));
  // more operations
  return jsonData.movies; // must be an array
});

const payload = {
  "title": "Aquaman 2",
  "description": "Um semideus rei dos oceanos",
  "launchdate": "2023-12-25T23:59:59.948Z",
  "showtimes": [
    "T17:00", "T19:30", "T22:10"
  ]
}

export function setup() {
  const res = baseRest.post(ENDPOINTS.MOVIES_ENDPOINT, payload);
  baseChecks.checkStatusCode(res, 201)
  return { responseData : res.json() }
}


export default () => {
  let movieIndex = __ITER % data.length;
  let movie = data[movieIndex];
  console.log(movie);

  const urlRes = baseRest.post(ENDPOINTS.MOVIES_ENDPOINT, movie);
  baseChecks.checkStatusCode(urlRes, 201)
  sleep(1);
};

// export function teardown(responseData) {
//   const movieId = responseData.responseData._id;
//   const res = baseRest.del(ENDPOINTS.MOVIES_ENDPOINT + `${movieId}`);
//   baseChecks.checkStatusCode(res, 201);
// }