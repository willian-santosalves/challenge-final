// 1. init code
// inicializa variáveis, define options(VUS, duration e thresholds )

import { sleep } from "k6";

let counter = 1;

export function setup() {
    // 2. setup code (executa apenas 1 vez antes da Função Principal)
    console.log(`SETUP ${counter}`);
  }
  
  export default function () {
    // 3. VU code (ponto de entrada dos VUS, onde realizam testes/chamadas na API)
    console.log(`FUNÇÃO PRINCIPAL - ${counter} VU = ${__VU} ITER = ${__ITER}`);
    counter = counter + 1;
    sleep(1);
  }
  
  export function teardown() {
    // 4. teardown code (executa apenas 1 vez depois da Função Principal)
    console.log(`TEARDOWN ${counter}`)
  }
  