import { faker } from "@faker-js/faker/locale/pt_BR";
import fs from "fs";

const quantidade = process.argv[2] || 1;

const ingressos = [];

for(let i = 0; i < quantidade; i++) {
    const ingresso = {
        moveId: faker.string.uuid(),
        userId: faker.string.uuid(),
        seatNumber: faker.number.int({ min: 1, max: 60 }),
        price: faker.number.int({ min: 1, max: 100 }),
        showtime: faker.date.future(),
    }
    ingressos.push(ingresso)
}

const data = {
    tickets: ingressos
}

fs.writeFileSync('tickets.json', JSON.stringify(data, null, 2), error => {
    if(error) {
        console.error(error);
    }
});