import { faker } from "@faker-js/faker/locale/pt_BR";
import fs from "fs";

const quantidade = process.argv[2] || 1;

const filmes = [];

for(let i = 0; i < quantidade; i++) {
    const filme = {
        title: faker.person.firstName() + ": O Filme",
        description: faker.lorem.paragraph(),
        launchdate: faker.date.past(),
        showtimes: [
            faker.date.future(), faker.date.future(), faker.date.future()
        ]
    }
    filmes.push(filme)
}

const data = {
    movies: filmes
}

fs.writeFileSync('movies.json', JSON.stringify(data, null, 2), error => {
    if(error) {
        console.error(error);
    }
});