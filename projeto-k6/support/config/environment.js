export const testConfig = {
    environment: {
        hml: {
            url: "http://localhost:3000/",
        },

        dev: {
            url: "http://localhost:3333",
        }
    },

    options: {
        smokeThresholds: {
            vus: 1, // Key for Smoke test. Keep it at 2, 3, max 5 VUs
            duration: '10s', // This can be shorter or just a few iterations
            thresholds: {
                http_req_duration: ['p(95) < 2000'],
                // http_req_failed: ['rate < 0.02']
            }
        },
    
        vinteFilmes1Minuto: {
            vus: 1,
            duration: '1s',
            thresholds: {
                http_req_duration: ['p(95) < 2000'],
                // http_req_failed: ['rate < 0.01']
            },
    
            // stages: [
            //     { duration: '30s', target: 10 },
            //     { duration: '30s', target: 20 }
            // ]
        },

        smokeTest: {
            vus: 1,
            duration: '1m'
        },

        loadTest: {
            stages: [
                { duration: '3m', target: 100 }, // traffic ramp-up from 1 to 100 users over 5 minutes.
                { duration: '10m', target: 100 }, // stay at 100 users for 30 minutes
                { duration: '3m', target: 0 }, // ramp-down to 0 users
            ]
        },

        stressTest: {
            stages: [
                { duration: '10m', target: 200 }, // traffic ramp-up from 1 to a higher 200 users over 10 minutes.
                { duration: '15m', target: 200 }, // stay at higher 200 users for 30 minutes
                { duration: '10m', target: 0 }, // ramp-down to 0 users
            ]
        }
    }
}