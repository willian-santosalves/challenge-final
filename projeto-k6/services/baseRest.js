import http from 'k6/http';
import { BaseService } from "./baseService.js";

export class BaseRest extends BaseService {
    constructor(base_uri) {
        super(base_uri);
    }

    post(endpoint, body, headers = {}, params = {}) {
        let uri = this.base_uri + endpoint
        let options = this.buildOptions(headers, params)
        return http.post(uri, JSON.stringify(body), options)
    }

    get(endpoint, _id, headers = {}, params = {}) {
        let res = this.base_uri + endpoint + _id;
        let options = this.buildOptions(headers, params)
        return http.get(uri, JSON.stringify(res._id), options);
    }

    put(endpoint, _id, headers = {}, params = {}) {
        let uri = this.base_uri + endpoint + _id;
        let options = this.buildOptions(headers, params)
        return http.put(uri, JSON.stringify(data), { headers: headers }, options);

        console.log(JSON.parse(res.body).json.name);
    }

    del(endpoint, _id, headers = {}, params = {}) {
        let uri = this.base_uri + endpoint + _id;
        let options = this.buildOptions(headers, params)
        return http.del(uri, JSON.stringify, options);
    }

    buildOptions(headers = {}, params = {}) {
        return {
            headers: Object.assign({'Content-Type': 'application/json'}, headers),
            params: Object.assign({}, params)
        }
    }
}