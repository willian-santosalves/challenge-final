[![Static Badge](https://img.shields.io/badge/API-NestJS--Cinema-green?style=flat)](https://github.com/juniorschmitz/nestjs-cinema)

# Challenge-Final | API Nestjs-Cinema

# Testes de Performance com Javascript utilizando **K6**

---

# Indice

- [Sobre](#-sobre)
- [Requisitos](#-requisitos)
- [Como baixar o projeto](#-como-baixar-o-projeto)
- [Instalando Gerenciador de pacotes, Bibliotecas e Ferramentas](#-instalando-gerenciador-de-pacotes-bibliotecas-e-ferramentas)
- [Rodando a API Nestjs-Cinema (localhost)](#rodando-a-api-nestjs-cinema-localhost)
- [Executando os Testes](#-rodando-os-testes)
- [Construído com](#-construido-com)
- [Referências e Referências de README](#referências-e-referências-de-readme)

---

## Sobre

- É um projeto de Teste de Performance tendo como linguagem de progrmação o Javascript, utilizando a API Serverest para estudos e a ferramenta de testes **K6**.

- O K6 é uma ferramenta de código aberto e serviço em nuvem que facilita o teste de carga para desenvolvedores e engenheiros de controle de qualidade.

---

## Requsitos

Você precisará ter essas ferramentas instaladas um sua máquina.

- Acesso a [API Nestjs-Cinema](https://github.com/juniorschmitz/nestjs-cinema)
- [Visual Studio Code](https://code.visualstudio.com/)
- [Node.js](https://nodejs.org/en)
- [Chocolatey Software](https://chocolatey.org/)
- [K6](https://k6.io/)
- [Faker.js](https://fakerjs.dev/)

---

## Como baixar o projeto

```bash
    #Abrir o Terminal

    # Clonar o repositório
    git clone https://gitlab.com/willian-santosalves/challenge-final.git

    # Entrar no diretório
    cd challenge-final

    cd projeto-k6

    #Iniciar o npm e diretório node_modules
    npm init
```

---

## Instalando Gerenciador de pacotes, Bibliotecas e Ferramentas

## Chocolatey

- É necessário instalar o gerenciador de pacotes Chocolatey para instalar o K6 no computador e rodar os testes.

    1. [Chocolatey Install](https://chocolatey.org/install)

    2. Abrir o Terminal do PowerShell em modo Administrador, copiar o comando da página de instalação do chocolatey, colar no terminal e prosseguir com a instalação.

    ```bash
        Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
    ```

---

## K6

- Depois de instalado o chocolatey, instalar o [K6](https://k6.io/docs/get-started/installation/)

    - No Windows

    ```bash
        choco install k6
    ```

---

## Biblioteca Faker.js

- Esta biblioteca é necessária para geração de massa de dados fictícias para a validação dos testes.

- Ir até a página do prjeto do Faker, Get Started, na seção Installation, terá a linha de comando para fazer a instalação da biblioteca ou pode ser copiado daqui mesmo.

- Este comando deve ser rodado pelo terminal dentro da pasta ou diretório raiz do projeto.

```bash
    # Entrar no diretório
    cd challenge-final

    cd projeto-k6

    #Instalar o Faker.js
    npm install @faker-js/faker --save-dev
```

---

## Rodando a API Nestjs-Cinema (localhost)

- [README da Nestjs Cinema](https://github.com/juniorschmitz/nestjs-cinema)

- Para rodar a API localmente é necessário executar o comando a seguir dentro da pasta do projeto da API.

```bash
    npm run start
```

- Basta abrir o navegador de sua preferência, digitar *localhost* na barra de pesquisa url do navegador, que abrirá a página da API rodando localmente *http://localhost:3000*. A Documentação em Swageer da API fica no endereço *https://localhost:3000/api/docs*, onde é possível encontar as rotas e os verbos http para executar os testes de consulta, cadastro, alteração e exclusão de dados na API. Ademais, a rota para consultar os filmes cadastrados é *https://localhost:3000/movies* e para consultar ingressos criados é *https://localhost:3000/tickets*.

---

## Executando os Testes

```bash
    #Para rodar o script dos testes
    k6 run caminho-da-pasta/nome-do-arquivo-do-teste.js

    #ou

    k6 run nome-do-arquivo-de-teste.js
```

--- 

## Construído com

- [Javascript](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript)

---

## Referências e Referências de README

- [K6](https://k6.io/)
- [Chocolatey Software](https://chocolatey.org/)
- [Meu Próprio Projeto no GitHub](https://github.com/willian-santosalves/alem-de-marte)
- [lohhans/README-PTBR.md](https://gist.github.com/lohhans/f8da0b147550df3f96914d3797e9fb89)
- [README da API Nestjs-Cinema](https://github.com/juniorschmitz/nestjs-cinema)